CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Converter Tools is a quick and easy way to perform several automatic
conversions.

It contains several tools like:

Text to Uppercase
Text to Lowercase
Get Only Letters
Get Only Numbers
Base 64 Encode
Base 64 Decode
SHA-1 Encode


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-8 for further
information.


CONFIGURATION
-------------

 * Access converter tools in Configuration » Development » Converter Tools. 


MAINTAINERS
-----------

Current maintainers:
 * Renato Gonçalves (RenatoG) - https://www.drupal.org/user/3326031
